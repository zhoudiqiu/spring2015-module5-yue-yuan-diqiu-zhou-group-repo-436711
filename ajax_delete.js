function deleteAjax(event){
	var username = document.getElementById("username").value;
	var title = document.getElementById("deleteTitle").value;
	var token = document.getElementById("token").value;
	var date = document.getElementById("date").value;
	var month = document.getElementById("month").value;
	var year = document.getElementById("year").value;
	var dataString = "username=" + encodeURIComponent(username) + "&title=" + encodeURIComponent(title) + "&token="+ encodeURIComponent(token) + "&date=" + encodeURIComponent(date) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "delete_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
	var jsonData = JSON.parse(event.target.responseText);
	if(jsonData.success){
		alert("You've deleted the event!");
			//show event
			var dataString = "username=" + encodeURIComponent(username) + "&token="+ encodeURIComponent(token) + "&date=" + encodeURIComponent(date) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "getData_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", getNoteCallback, false);
	xmlHttp.send(dataString);

function getNoteCallback(event){
	//to be completed
	var jsonData = JSON.parse(event.target.responseText);
	if(jsonData.success){
		//var eventString=jsonData.text;
		//var titleString=jsonData.title;
		var ul=document.getElementById("eventList");
		var i=0;
		for( i=0; i<jsonData.eventarr.length; i++){
			titleString = jsonData.eventarr[i].title;
			eventString = jsonData.eventarr[i].text;
			ul.innerHTML="<li>"+"title: "+titleString+"<br>"+"event: "+eventString+"</li>";
		}
		
	}
	
}
//end of show event
		}else{
			alert("Deletion process failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(dataString);
}
document.getElementById("delete_btn").addEventListener("click", deleteAjax, false);