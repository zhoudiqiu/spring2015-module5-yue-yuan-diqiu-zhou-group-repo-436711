<?php
ini_set("session.cookie_httponly", 1);
require 'database.php';
header("Content-Type: application/json");
$username=$_POST['username'];

$stmt = $mysqli->prepare("SELECT username,crypted_password FROM loginfo WHERE username=?");
if(!$stmt){
    echo json_encode(array(
        "success" => false,
        "message" => $mysqli->error
        ));
    }
            // Bind the parameter
$stmt->bind_param('s', $username);
$stmt->execute();
 
            // Bind the results
$stmt->bind_result($user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = $_POST['password'];
            // Compare the submitted password to the actual password hash
if( crypt($pwd_guess, $pwd_hash)==$pwd_hash){

    session_start();
    $_SESSION['username'] = $username;
    $_SESSION['token'] = substr(md5(rand()),0,10);
    //maybe need a header location

    echo json_encode(array(
	"success" => true,
    "message"=>"You've logged in!",
    "token" => $_SESSION['token']
	));
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect Username or Password"
	));
	exit;
}
?>