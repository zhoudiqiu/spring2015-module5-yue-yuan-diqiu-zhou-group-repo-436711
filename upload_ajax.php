<?php
require 'database.php';
header("Content-Type: application/json");
$token=$_POST['token'];
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['username']==$username && $_SESSION['token']==$token){
	$year = $_POST['year'];
	$month = $_POST['month'];
	$date = $_POST['date'];
	$text = $_POST['text'];
	$title= $_POST['title'];
	$stmt=$mysqli->prepare("insert into event(author,title,text,date,month,year) values (?,?,?,?,?,?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		echo json_encode(array(
		"success" => false,
		"message" => "Permission denied"
	));
		exit;
	}
	$stmt->bind_param('sssiii', $username,$title,$text,$date,$month,$year);
	$stmt->execute();
	$stmt->close();
	echo json_encode(array(
		"success" => true
		));
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>