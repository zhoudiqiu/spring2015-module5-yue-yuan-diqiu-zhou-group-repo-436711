<?php
require 'database.php';
header("Content-Type: application/json");
$token=$_POST['token'];
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['token']==$token){
	$year = (int)htmlentities($_POST['year']);
	$month = (int)htmlentities($_POST['month']);
	$date = (int)htmlentities($_POST['date']);
	$title = htmlentities($_POST['title']);
	$newText = htmlentities($_POST['newText']);
	//query needs to be modified to replace the old text with the new text
	$stmt=$mysqli->prepare("update event set text=? where title=? and year=? and month=? and date=? and author=? ");//Query needs change;
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"message" => "Incorrect query"
		));
	};
	//also needs modified with query
	$stmt->bind_param('ssiiis', $newText,$title,$year,$month,$date,$_SESSION['username']);
	$stmt->execute();
	$stmt->close();
	echo json_encode(array(
		"success" => true
		));
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>