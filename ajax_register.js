function registerAjax(event){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	var dataString = "username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "register_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			var token=document.getElementById("token");
			token.innerHTML=jsonData.token;
			alert("You've sucessfully registered and logged in!");
		}else{
			alert("Registration process failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(dataString);
}
document.getElementById("register_btn").addEventListener("click", registerAjax, false);