function uploadAjax(event){
	var username = document.getElementById("username").value;
	var token = document.getElementById("token").value;
	var date = document.getElementById("date").value;
	var month = document.getElementById("month").value;
	var year = document.getElementById("year").value;
	var text = document.getElementById("text").value;
	var title=document.getElementById("addTitle").value;
	alert(date+"/"+month+"/"+year+text+"/"+title);
	alert(token);
	var dataString = "username=" + encodeURIComponent(username) + "&title="+encodeURIComponent(title)+"&text=" + encodeURIComponent(text) + "&token="+ encodeURIComponent(token) + "&date=" + encodeURIComponent(date) + "&month=" + encodeURIComponent(month) + "&year=" + encodeURIComponent(year);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "upload_ajax.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load",  function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			alert("You've uploaded the new event!");
		}else{
			alert("Upload process failed"+jsonData.message);
		}
	},false);
	xmlHttp.send(dataString);
}
document.getElementById("upload_btn").addEventListener("click", uploadAjax, false);