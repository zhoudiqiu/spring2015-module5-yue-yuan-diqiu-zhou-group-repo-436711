<?php
require 'database.php';
header("Content-Type: application/json");
$username=$_POST['username'];
$token=htmlentities($_POST['token']);
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['username']==$username && $_SESSION['token']==$token){
	$viewername=$_POST['viewername'];
	$year = (int)htmlentities($_POST['year']);
	$month = (int)htmlentities($_POST['month']);
	$date =(int)htmlentities($_POST['date']);
	$stmt=$mysqli->prepare("update event set viewer=? where year=? and month=? and date=? and author=?");//Query needs change;
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		echo json_encode(array(
		"success" => false,
		"message"=>"the viewer already exists"
		));
		exit;
	}
	$stmt->bind_param('siiis',$viewername, $year,$month,$date,$_SESSION['username']);
	$stmt->execute();
	$stmt->close();
	echo json_encode(array(
		"success" => true
		));
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>