<?php
require 'database.php';
header("Content-Type: application/json");
$username=$_POST['username'];
$token=$_POST['token'];
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['username']==$username && $_SESSION['token']==$token){
	$year = $_POST['year'];
	$month = $_POST['month'];
	$date = $_POST['date'];
	$title = $_POST['title'];
	$stmt=$mysqli->prepare("delete from event where author=? and title=? and date=? and month=? and year=?");
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"message" => "Permission denied"
		));		
		exit;
	}
	$stmt->bind_param('ssiii', $_SESSION['username'],$title,$date,$month,$year);
	$stmt->execute();
	$stmt->close();
	echo json_encode(array(
		"success" => true,
		"message"=> "Deleted"
		));
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>