<?php
require 'database.php';
header("Content-Type: application/json");
$token=htmlentities($_POST['token']);
ini_set("session.cookie_httponly", 1);
session_start();
if($_SESSION['token']==$token){
	$year = (int)htmlentities($_POST['year']);
	$month = (int)htmlentities($_POST['month']);
	$date = (int)htmlentities($_POST['date']);
	$stmt=$mysqli->prepare("select tag from event where year=? and month=? and date=? and (author=? or viewer=?)");
	//query needs to be modified;
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"exist"=>false,
			"message" => "No event"
		));
		exit;
	}
	$stmt->bind_param('iiiss', $year,$month,$date,$_SESSION['username'],$_SESSION['username']);
	$stmt->execute();
	$stmt->bind_result($tag);

	while($stmt->fetch()){

		echo json_encode(array(
		"success" => true,
		"tag"=>$tag
		));
	}	
	$stmt->close();
	exit;
}else{
	echo json_encode(array(
		"success" => false,
		"message" => "Incorrect user or CSRF token"
	));
	exit;
}
?>